package tictactoeagain;

public class HumanPlayer extends AbstractPlayer{

    public HumanPlayer(PlayerType p) {
        super(p);
    }

    
    @Override
    public Cell nextMove(Board b) {
        int row, col;
        System.out.println( myType.toString() + " játékos köre. Adja meg a következö lépését.");
        while( true ){
            System.out.print("sor: ");
            row = UserInterface.readInt(0, b.ROWS-1);
            System.out.print("oszlop: ");
            col = UserInterface.readInt(0, b.COLUMNS-1);
            if( b.isValidCell(row, col) && b.getCell(row, col).equals(PlayerType.EMPTY) ){
                break;
            }else{
                System.out.println("Rossz mezöt adott meg.");
            }
        }
        return new Cell(row, col, myType);
    }
    
}
