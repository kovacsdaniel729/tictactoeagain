package tictactoeagain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/*
A GameTree belsö osztály objektumaival mint csúcsokkal épít döntési fát. Ami 
elég idö- és memóriaigényes tud lenni, ezért idökorlátot szabtam.

Ellenörzésként a részeredményeket fájlba lehet írni. A nextMove metódusban 
szerepelnek a kikommentezett printGameTree és printChild metódusok, amik ezt 
elvégzik.
*/
public class MinimaxPlayer extends AbstractPlayer{

    //a fájlíráshoz
    private PrintStream pw;
    private final String fileName;
    
    protected Board currentBoard;
    protected List<GameTree> steps = new ArrayList<>();     //a döntési fa csúcsainak listája
    protected final int TREE_LEVEL_LIMIT;
    protected final int BOARD_MAX_VALUE = 10000000;
    
    private int numberOfSteps=0;
    protected int startValue;
    protected long startTime;
    protected final long TIME_LIMIT;    // 0 = nincs limit
    protected boolean timeout;
    
    public MinimaxPlayer(PlayerType p){
        super(p);
        fileName = "src/player" + p.toString() + ".txt";
        try{
            pw = new PrintStream(new File(fileName));
        }catch(FileNotFoundException ex){
            System.out.println(ex);
        }
        TREE_LEVEL_LIMIT = 4;
        TIME_LIMIT = 1000*20L;
    }

    public MinimaxPlayer(PlayerType p, int timeout, int difficulty) {
        super(p);
        pw = null;
        fileName = null;
        TIME_LIMIT = (long)(timeout * 1000);
        TREE_LEVEL_LIMIT = difficulty * 2;
    }    
    
    public void endGame(){
        numberOfSteps=0;
        if(pw!=null){pw.close();};
        currentBoard = null;
        steps.clear();
    }
    
    @Override
    public Cell nextMove(Board b) {
        numberOfSteps++;
        timeout = false;
        List<Cell> emptyCells = b.emptyCells();
        if(emptyCells.isEmpty()){
            return null;
        }
        steps.clear();
        currentBoard = b.getCopy();
        if( numberOfSteps==1 && 
                countPlayerMark(currentBoard, myType) == 0 &&
                countPlayerMark(currentBoard, myType.invert())  == 0 ){
            return new Cell(currentBoard.ROWS/2, currentBoard.COLUMNS/2, myType);
        }
        GameTree root = new GameTree(b,-1, -1, myType.invert(), 0);
        steps.add(root);
        startTime = System.currentTimeMillis();
        //startValue = calcBoardValue(root.board);
        makeGameTreeBFS();
        Minmax(root);
        
        //printGameTree(root);      //ez a teljes fát kiírja ,ami elég nagy tud lenni, de néha hasznos tud lenni
        //printChild(root);         //ez csak a következö, lehetséges lépéseket írja ki
        
        //megkeressük és visszaadjuk azt a lépést, ami a maximumot adta
        for (GameTree child : root.children) {
            if(child.value == root.value){
                return new Cell(child.row, child.column, myType);
            }
        }
        return null;    //ide csak akkor szabad eljutnia, ha betelt a tábla
    }
    
    
    //döntési fa felépítése (szélességi bejárás szerint)
    protected void makeGameTreeBFS(){
        while(!steps.isEmpty()){
            GameTree parent = steps.get(0);
            steps.remove(0);
            if(parent.isCalculated){continue;}
            //Board b = parent.getBoard();
            List<GameTree> currentSteps = new ArrayList<>();
            List<Cell> currentEmptyCells = trimmedEmptyCells( parent.getBoard() );
            for (Cell cell : currentEmptyCells ) {//b.emptyCells()
                //if( parent.getPreviousCells().contains(cell) ){continue;}// isAway(b, cell)
                GameTree current = new GameTree(parent, cell.getRow(), cell.getCol());
                parent.addChild(current);
                boolean isWinner = current.getBoard().hasWon(parent.player);
                //ha meg tudja nyerni, azt választja, és nem nézi a többit
                if(isWinner){
                    currentSteps.clear();
                    current.evaluate();
                    steps.add(current);
                    break;
                }
                //int currentValue = calcBoardValue(current.board);
                if (current.level < TREE_LEVEL_LIMIT) {
                    currentSteps.add(current);
                } else {
                    current.evaluate();
                }
            }
            steps.addAll(currentSteps);
            //parent.board = null;
            if((System.currentTimeMillis() - startTime) > TIME_LIMIT && TIME_LIMIT != 0){
                timeout = true;
                return;
            }
        }
    }
    
    protected void Minmax(GameTree parent){
        if( !parent.isCalculated ){
            for (GameTree child : parent.children) {
                Minmax(child);
            }
            if( parent.player.equals(myType) ){
                parent.setToChildMax();
            }else{
                parent.setToChildMin();
            }
        }
    }
    
    //végleges érték, a játékos helyzetének értékének és az ellenség helyzetének értékének különbsége
    protected int calcBoardValue(Board b){
        //int myValue =  b.getSomeValue(myType), enemyValue = b.getSomeValue(myType.invert());
        int myValue = calcBoardPlayerValue(b,myType), enemyValue = calcBoardPlayerValue(b,myType.invert());
        if( b.hasWon(myType.invert()) ){
            return -BOARD_MAX_VALUE + countPlayerMark(currentBoard, myType);
        }
        if( b.hasWon(myType) ){
            return BOARD_MAX_VALUE - countPlayerMark(currentBoard, myType.invert());
        }
        return myValue - enemyValue;
    }
    
    //metódusok, amik a részeredményeket fájlba írják
    protected void printChild(GameTree node){
        if(pw==null){return;}
        pw.println("-----------------------------------");
        pw.println( numberOfSteps+ " ---------------------------------");
        pw.println("-----------------------------------");
        if( timeout ){
            pw.println("time limit exceeded");
        }
        pw.println("row=" + node.row + ", col=" + node.column + ", value=" + node.value);
        for (GameTree child : node.children) {
             pw.println("\trow=" + child.row + ", col=" + child.column + ", value=" + child.value);
        }
        pw.println();
    }
    
    protected void printGameTree(GameTree root){
        if(pw==null){return;}
        pw.println("-----------------------------------");
        pw.println( numberOfSteps+ " ---------------------------------");
        pw.println("-----------------------------------");
        if( timeout ){
            pw.println("time limit exceeded");
        }
        printGameTree(root, 0);
    }
    
    private void printGameTree(GameTree node, int level){
        pw.println("row=" + node.row + ", col=" + node.column + ", value=" + node.value);
        for (GameTree child : node.children) {
            for (int i = 1; i <= level; i++) {
                pw.print("\t");
            }
            printGameTree(child, level+1);
        }
    }
    
    //szétszedi a táblát sorokra, oszlopokra, átlókra, és azok részereményét összesíti
    protected int calcBoardPlayerValue(Board b, PlayerType p){
        int value = 0;
        //sorok
        for (int i = 0; i < b.ROWS; i++) {
            PlayerType[] row = new PlayerType[b.COLUMNS];
            for (int j = 0; j < b.COLUMNS; j++) {
                row[j] = b.getCell(i, j);
            }
            value += calcPartialValue(row,p);
        }
        //oszlopok
        for (int j = 0; j < b.COLUMNS; j++) {
            PlayerType[] col = new PlayerType[b.ROWS];
            for (int i = 0; i < b.ROWS; i++) {
                col[i] = b.getCell(i, j);
            }
            value += calcPartialValue(col,p);
        }
        //átlók
        //diagonals = rows + cols - 1
        for (int i = b.NUMBER_OF_MARKS-1; i < b.ROWS + b.COLUMNS - b.NUMBER_OF_MARKS ; i++) {
            int startRow = Math.max(b.ROWS-1-i, 0), startCol = Math.max(i-b.ROWS+1, 0);
            PlayerType[] diag = new PlayerType[Math.min(b.ROWS-startRow, b.COLUMNS-startCol)];
            for (int j = 0; j < Math.min(b.ROWS-startRow, b.COLUMNS-startCol); j++) {
                diag[j] = b.getCell(startRow+j, startCol+j);
            }
            value += calcPartialValue(diag,p);
        }
        //mellékátlók
        //vertically mirror, col -> COLUMNS - col - 1
        for (int i = b.NUMBER_OF_MARKS-1; i < b.ROWS + b.COLUMNS - b.NUMBER_OF_MARKS ; i++) {
            int startRow = Math.max(b.ROWS-1-i, 0), startCol = Math.max(i-b.ROWS+1, 0);
            PlayerType[] diag = new PlayerType[Math.min(b.ROWS-startRow, b.COLUMNS-startCol)];
            for (int j = 0; j < Math.min(b.ROWS-startRow, b.COLUMNS-startCol); j++) {
                diag[j] = b.getCell(startRow+j, b.COLUMNS - startCol-j - 1);
            }
            value += calcPartialValue(diag,p);
        }
        return value;
    }
    
    //érték ~~ sorozat hosszának négyzete, nem vesszük figyelembe a tábla adatait
    /*
    protected int calcPartialValue( PlayerType[] array, PlayerType p){
        int count=0;
        int value=0;
        boolean wasEmpty = false;
        for (PlayerType type : array) {
            if( type.equals(PlayerType.EMPTY) ){
                if(count > 0){
                    value += (count+1) * ( (wasEmpty) ? (count+1) : (count) );
                    count=0; 
                }
                wasEmpty = true;
            }else if( type.equals(p) ){
                count++;
            }else{
                value += (wasEmpty) ? count * count : 0;
                count=0;
                wasEmpty = false;
            }
        }
        value += (wasEmpty) ? count * count : 0;
        return value;
    }
    */
    
    //érték ~~ sorozat hosszának négyzete, figyelembe vesszük a tábla adatait
    protected int calcPartialValue( PlayerType[] array, PlayerType p){
        int count=0;
        int value=0;
        int mark = currentBoard.NUMBER_OF_MARKS;
        boolean wasEmpty = false;
        for (PlayerType type : array) {
            if( type.equals(PlayerType.EMPTY) ){
                if(count >= mark){
                    return BOARD_MAX_VALUE;
                }else if( count == mark-1 && wasEmpty){
                    value += BOARD_MAX_VALUE/10;
                    count = 0;
                }else if(count > 1){
                    value += (count+1) * ( (wasEmpty) ? (count+1) : (count) );
                    count=0; 
                }else if(count == 1){
                    value += 1;
                    count = 0;
                }
                wasEmpty = true;
            }else if( type.equals(p) ){
                count++;
            }else{
                if(count >= mark){
                    return BOARD_MAX_VALUE;
                }
                value += (wasEmpty) ? count * count : 0;
                count=0;
                wasEmpty = false;
            }
        }
        //a tábla szélén lévőt is kiértékeljük
        if(count >= mark){
            return BOARD_MAX_VALUE;
        }
        value += (wasEmpty) ? count * count : 0;
        return value;
    }
    
    public int countPlayerMark(Board b, PlayerType p){
        int count = 0;
        for (int i = 0; i < b.ROWS; i++) {
            for (int j = 0; j < b.COLUMNS; j++) {
                if(b.getCell(i, j).equals(p)){
                    count++;
                }
            }
        }
        return count;
    }
    
    
    protected boolean isAway(Board b, Cell c){
        int count=0;    //szomszédos X és O mezök száma
        int row = c.getRow(), col = c.getCol();
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if ( b.isValidCell(row+i, col+j) && !b.getCell(row + i, col + j).equals(PlayerType.EMPTY)) {
                    count++;
                }
            }
        }
        return (count == 0);
    }
    
    //csak a közeli üres mezök
    protected List<Cell> trimmedEmptyCells(Board b){
        List<Cell> emptyCells = b.emptyCells();
        Iterator<Cell> iter = emptyCells.iterator();
        while (iter.hasNext()) {
            if( isAway( b, iter.next() ) ){
                iter.remove();
            }            
        }
        return emptyCells;
    }
    
    
    
    protected class GameTree{
        
        public final PlayerType player;     //a játékos, aki következne, azaz az ellenfél rakott egyet (row,col)-ra
        public final byte level;
        public final byte row,column;
        public int value;
        public boolean isCalculated = false;
        public List<GameTree> children = new ArrayList<>();
        public GameTree parent = null;
        
        public GameTree(Board b, int row, int column, PlayerType player, int level ){
            this.row = (byte)row;
            this.column = (byte)column;
            this.player = player.invert();  //itt INVERTÁLUNK
            this.level = (byte)level;
        }
        
        public GameTree(GameTree parent, int row, int column){
            this.row = (byte)row;
            this.column = (byte)column;
            this.player = parent.player.invert();   //itt INVERTÁLUNK
            level = (byte)(parent.level+1);
            this.parent = parent;
        }
        
        public void addChild(GameTree child){
            children.add(child);
        }
        
        public void setToChildMin(){
            int min = Integer.MAX_VALUE;
            for (GameTree child : children) {
                if( child.value < min ){
                    min = child.value;
                }
            }
            if( children.isEmpty() ){
                evaluate();
            }else{
                setValue(min);
            }
            //setValue( children.isEmpty() ? calcBoardValue(board) : min);
        }
        
        public void setToChildMax(){
            int max = Integer.MIN_VALUE;
            for (GameTree child : children) {
                if( child.value > max ){
                    max = child.value;
                }
            }
            if( children.isEmpty() ){
                evaluate();
            }else{
                setValue(max);
            }
            //setValue( children.isEmpty() ? calcBoardValue(board) : max);
        }
        
        public void setValue(int value){
            this.value = value;
            isCalculated = true;
            //board = null;
        }
        
        public void evaluate(){
            Board imagBoard = currentBoard.getCopy();
            GameTree gt = this;
            while( gt != null ){
                imagBoard.put( new Cell(gt.row, gt.column, gt.player.invert() ) );
                gt = gt.parent;
            }
            setValue( calcBoardValue( imagBoard ) );
        }
        
        //clone
        public Board getBoard(){
            Board imagBoard = currentBoard.getCopy();
            GameTree gt = this;
            while( gt != null ){
                imagBoard.put( new Cell(gt.row, gt.column, gt.player.invert() ) );
                gt = gt.parent;
            }
            return imagBoard;
        }
        
        public List<Cell> getPreviousCells(){
            ArrayList<Cell> cells = new ArrayList<>();
            GameTree gt = this;
            while( gt != null ){
                cells.add(new Cell(gt.row, gt.column, gt.player.invert() ) );
                gt = gt.parent;
            }
            return cells;
        }
    }
    //-------------------------  end of GameTree  -----------------------------
    
    
}
