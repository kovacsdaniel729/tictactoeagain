package tictactoeagain;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
Ez valósítja meg a menürenszer. Statikus inicializálóban példányosítjuk az 
osztály példányait, amelyek az egyes menüpontoknak felelnek meg.
*/

public class UserInterface {

    public static int boardRow = 3;
    public static int boardColumn = 3;
    public static int boardMark = 3;
    public static boolean isHuman1 = false;
    public static boolean isHuman2 = false;
    public static int ai1Difficulty = 2;
    public static int ai2Difficulty = 2;
    public static int ai1TimeOut = 20;
    public static int ai2TimeOut = 20;
    public static boolean isForceQuit = true;
    
    private static boolean isFinished;
    private static final Scanner sc = new Scanner(System.in, "windows-1252");
    //private static final Scanner sc = new Scanner(System.in, "utf-8");
    private static final int EXIT_INT = 0;
    
    private static UserInterface root;
    private static UserInterface currentUI;
    
    
    private String menuName;        //a menü rövid elnevezése
    private UserInterface parent;   //a felette álló menüpont
    private List<UserInterface> children = new ArrayList<>();   //az alatta álló menüpontok
    
    static{
        root = new UserInterface("Fömenü", null){
            @Override
            protected void doSomething(){
                System.out.println("Ez a fömenü. Válasszon almenüt.");
            }
        };
        
        UserInterface inputBoard = new UserInterface("Tábla beállítása", root){
            @Override
            protected void doSomething(){
                System.out.println("Adja meg a sorok számát! (3-100)");
                boardRow = readInt(3, 100);
                System.out.println("Adja meg az oszlopok számát! (3-100)");
                boardColumn = readInt(3, 100);
                System.out.println("Adja meg, hogy hány jel kelljen a nyeréshez! (3-100)");
                boardMark = readInt(3, 100);
            }
        };
        root.addChild(inputBoard);
        
        UserInterface settingsPlayer1 = new UserInterface("Első játékos beállításai (X)", root){
            @Override
            protected void doSomething(){
                System.out.println("Első játékos beállításai (X)");
            }
        };
        root.addChild(settingsPlayer1);
        
        UserInterface inputHuman1 = new UserInterface("Ember / Gép", settingsPlayer1){
            @Override
            protected void doSomething(){
                System.out.println("Első játékos ember legyen?");
                isHuman1 = readBoolean();
            }
        };
        settingsPlayer1.addChild(inputHuman1);
        
        UserInterface inputDifficulty1 = new UserInterface("Nehézségi szint", settingsPlayer1){
            @Override
            protected void doSomething(){
                System.out.println("Adja meg az első gépi játékos nehézségi szintjét! (1-2)");
                ai1Difficulty = readInt(1, 2);
            }
        };
        settingsPlayer1.addChild(inputDifficulty1);
        
        UserInterface inputTimeOut1 = new UserInterface("Időkorlát", settingsPlayer1){
            @Override
            protected void doSomething(){
                System.out.println("Adja meg az első gépi játékos időkorlátját másodpercben! (1-60, 0 = nincs korlát)");
                ai1TimeOut = readInt(0,60);
            }
        };
        settingsPlayer1.addChild(inputTimeOut1);
        
        UserInterface settingsPlayer2 = new UserInterface("Második játékos beállításai (O)", root){
            @Override
            protected void doSomething(){
                System.out.println("Második játékos beállításai (O)");
            }
        };
        root.addChild(settingsPlayer2);
        
        UserInterface inputHuman2 = new UserInterface("Ember / Gép", settingsPlayer2){
            @Override
            protected void doSomething(){
                System.out.println("Második játékos ember legyen?");
                isHuman2 = readBoolean();
            }
        };
        settingsPlayer2.addChild(inputHuman2);
        
        UserInterface inputDifficulty2 = new UserInterface("Nehézségi szint", settingsPlayer2){
            @Override
            protected void doSomething(){
                System.out.println("Adja meg az második (gépi) játékos nehézségi szintjét! (1-2)");
                ai2Difficulty = readInt(1, 2);
            }
        };
        settingsPlayer2.addChild(inputDifficulty2);
        
        UserInterface inputTimeOut2 = new UserInterface("Időkorlát", settingsPlayer2){
            @Override
            protected void doSomething(){
                System.out.println("Adja meg az második (gépi) játékos időkorlátját másodpercben! (1-60, 0 = nincs korlát)");
                ai2TimeOut = readInt(0,60);
            }
        };
        settingsPlayer2.addChild(inputTimeOut2);
        
        UserInterface printData = new UserInterface("Paraméterek megjelenítése", root){
            @Override
            protected void doSomething(){
                System.out.println("Tábla: sorok száma " + boardRow + 
                        ", oszlopok száma " + boardColumn + 
                        ", nyeréshez szükséges jelek száma " + boardMark + ".");
                if(isHuman1){
                    System.out.println("Első játékos ember.");
                }else{
                    System.out.println("Első játékos gép. Nehézségi szint " + ai1Difficulty + 
                            ", időkorlát " + ai1TimeOut + " másodperc.");
                }
                if(isHuman2){
                    System.out.println("Második játékos ember.");
                }else{
                    System.out.println("Második játékos gép. Nehézségi szint " + ai2Difficulty + 
                            ", időkorlát " + ai2TimeOut + " másodperc.");
                }
            }
        };
        root.addChild(printData);
        
        UserInterface printMenu = new UserInterface("Menürendszer kiíratása", root){
            @Override
            protected void doSomething(){
                printStructure();
            }
        };
        root.addChild(printMenu);
        
        UserInterface playGame = new UserInterface("Játék indítása", root){
            @Override
            protected void doSomething(){
                isFinished = true;
                isForceQuit = false;
            }
        };
        root.addChild(playGame);
        
        UserInterface quit = new UserInterface("Kilépés", root){
            @Override
            protected void doSomething(){
                isFinished = true;
            }
        };
        root.addChild(quit);
        
        
    }
    
    
    

    private UserInterface(String menuName, UserInterface parent) {
        this.menuName = menuName;
        this.parent = parent;
    }
    
    public static void start(){
        currentUI = root;
        isFinished = false;
        while(!isFinished){
            currentUI.doSomething();
            currentUI = currentUI.nextUI();
            System.out.println("---------------------------------------------");
        }
    }
    
    protected void doSomething(){
    }
    
    private UserInterface getParent(){
        if( parent != null){
            return parent;
        }else{
            return this;
        }
    }
    
    private void addChild(UserInterface ui){
        children.add(ui);
    }
    
    private void printChildren(){
        for (int i = 0; i < children.size(); i++) {
            System.out.print( (i+1) + "=" + children.get(i).menuName + "\n");
        }
        System.out.println( EXIT_INT + "=fel");
    }
    
    protected UserInterface nextUI(){
        if(children.isEmpty()){
            return getParent();
        }
        System.out.println("almenük:");
        printChildren();
        int number = readInt(EXIT_INT, children.size());
        return (number == EXIT_INT) ? getParent() : children.get(number-1);
        /*
        if(number == exitInt){
            return previousUI;
        }else{
            return children.get(number);
        }
        */
    }
    
    public static int readInt(int lowerLimit, int upperLimit){
        while(true){
            String line = sc.nextLine();
            try {
                int number = Integer.parseInt(line);
                if( number >= lowerLimit && number <= upperLimit){
                    return number;
                }else{
                    System.out.println( lowerLimit + " és " + upperLimit + " között adjon meg számot.");
                }
            } catch (NumberFormatException ex) {
                System.out.println("Rendes számot adjon meg!");
            }
        }
    }
    
    public static int readInt(){
        return readInt(Integer.MIN_VALUE, Integer.MAX_VALUE);
    }
    
        
    public static boolean readBoolean(){
        while(true){
            System.out.println("1=igen, 2=nem");
            String line = sc.nextLine();
            try {
                int number = Integer.parseInt(line);
                if( number ==1 || number == 2){
                    return number==1;
                }else{
                    System.out.println("1-et vagy 2-t adjon meg!");
                }
            } catch (NumberFormatException ex) {
                System.out.println("Rendes számot adjon meg!");
            }
        }
    }
    
    public static void printStructure(){
        System.out.println("Menüszerkezet:");
        printStructure(root, 0);
    }
    
    private static void printStructure(UserInterface ui, int level){
        for (UserInterface child : ui.children) {
            for (int i = 0; i < level; i++) {
                System.out.print("\t");
            }
            System.out.println( child.menuName );
            printStructure(child, level+1);
        }
    }
    
}
