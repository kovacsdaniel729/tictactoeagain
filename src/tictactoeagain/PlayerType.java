package tictactoeagain;

/**
 * @author pappgergely
 */
public enum PlayerType {
    // The empty value means that that cell is not occupied by a player
    O, X, EMPTY;
    
    public PlayerType invert(){
        switch(this){
            case O:
                return X;
            case X:
                return O;
            default:
                return EMPTY;
        }
    }
}
