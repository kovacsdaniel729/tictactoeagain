package tictactoeagain;

import java.util.ArrayList;
import java.util.List;

/*
Már nem interface, hanem rendes osztály. Tetszöleges méretü lehet a tábla. Már 
nem dob kivételeket, ha rossz mezöt adunk meg. Helyette booleannal tér vissza 
aszerint, hogy érvényes mezöt adunk-e meg.
*/
public class Board {

    public final int ROWS;
    public final int COLUMNS;
    public final int NUMBER_OF_MARKS;    //ennyi kell a nyeréshez

    protected PlayerType[][] cells;

    public Board() {
        this(3, 3, 3);  //standard 3x3 Tic-Tac-Toe
    }

    public Board(int ROWS, int COLUMNS, int NUMBER_OF_MARKS) {
        this.ROWS = ROWS;
        this.COLUMNS = COLUMNS;
        this.NUMBER_OF_MARKS = NUMBER_OF_MARKS;
        cells = new PlayerType[ROWS][COLUMNS];
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                cells[i][j] = PlayerType.EMPTY;
            }
        }
    }

    public boolean isValidCell(int rowIdx, int colIdx) {
        return rowIdx >= 0 && rowIdx < ROWS && colIdx >= 0 && colIdx < COLUMNS;
    }

    public boolean isValidCell(Cell cell) {
        return isValidCell(cell.getRow(), cell.getCol());
    }

    public PlayerType getCell(int rowIdx, int colIdx) {
        return cells[rowIdx][colIdx];
    }

    //már nem dob kivételt, hanem booleannal tér vissza, hogy érvényes volt-e
    public boolean put(Cell cell) {
        int rowIdx = cell.getRow(), colIdx = cell.getCol();
        if (isValidCell(cell) && cells[rowIdx][colIdx].equals(PlayerType.EMPTY)) {
            cells[rowIdx][colIdx] = cell.getCellsPlayer();
            return true;
        } else {
            return false;
        }
    }

    public boolean hasWon(PlayerType p) {
        //return (maxMarkLength(p) >= NUMBER_OF_MARKS);
        int count;
        for (int i = 0; i < ROWS; i++) {
            count = 0;
            for (int j = 0; j < COLUMNS; j++) {
                if (cells[i][j].equals(p)) {
                    count++;
                    if (count == NUMBER_OF_MARKS) {
                        return true;
                    }
                } else {
                    count = 0;
                }
            }
        }
        for (int j = 0; j < COLUMNS; j++) {
            count = 0;
            for (int i = 0; i < ROWS; i++) {
                if (cells[i][j].equals(p)) {
                    count++;
                    if (count == NUMBER_OF_MARKS) {
                        return true;
                    }
                } else {
                    count = 0;
                }
            }
        }
        //átlók száma = rows + cols - 1
        for (int i = NUMBER_OF_MARKS - 1; i < ROWS + COLUMNS - NUMBER_OF_MARKS; i++) {
            count = 0;
            int startRow = Math.max(ROWS - 1 - i, 0), startCol = Math.max(i - ROWS + 1, 0);
            for (int j = 0; j < Math.min(ROWS - startRow, COLUMNS - startCol); j++) {
                if (cells[startRow + j][startCol + j].equals(p)) {
                    count++;
                    if (count == NUMBER_OF_MARKS) {
                        return true;
                    }
                } else {
                    count = 0;
                }
            }
        }
        //függöleges tükrözés, col -> COLUMNS - col - 1
        for (int i = NUMBER_OF_MARKS - 1; i < ROWS + COLUMNS - NUMBER_OF_MARKS; i++) {
            count = 0;
            int startRow = Math.max(ROWS - 1 - i, 0), startCol = Math.max(i - ROWS + 1, 0);
            for (int j = 0; j < Math.min(ROWS - startRow, COLUMNS - startCol); j++) {
                if (cells[startRow + j][COLUMNS - startCol - j - 1].equals(p)) {
                    count++;
                    if (count == NUMBER_OF_MARKS) {
                        return true;
                    }
                } else {
                    count = 0;
                }
            }
        }
        return false;
    }

    public List<Cell> emptyCells() {
        List<Cell> emptyCells = new ArrayList<>();
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (cells[i][j].equals(PlayerType.EMPTY)) {
                    emptyCells.add(new Cell(i, j));
                }
            }
        }
        return emptyCells;
    }

    public void printBoard() {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                if (cells[i][j].equals(PlayerType.EMPTY)) {
                    System.out.print("·");
                } else {
                    System.out.print(cells[i][j].toString());
                }
            }
            System.out.println("");
        }
    }

    public void printNiceBoard() {
        System.out.print("\t");
            System.out.print("          ");
        for (int j = 1; j <= (COLUMNS-1)/10; j++) {
            System.out.print(10*j + "        ");
        }
        System.out.print("\n\t");
        for (int j = 0; j < COLUMNS; j++) {
            if (j%2 == 1 ) {
                System.out.print(j%10);
            }else{
                System.out.print(" ");
            }
        }
        System.out.print("\n\t");
        for (int j = 0; j < COLUMNS; j++) {
            if (j%2 == 0 ) {
                System.out.print(j%10 );
            }else{
                System.out.print(" ");
            }
        }
        System.out.println("");
        for (int i = 0; i < ROWS; i++) {
            System.out.print(i + "\t");
            for (int j = 0; j < COLUMNS; j++) {
                if (cells[i][j].equals(PlayerType.EMPTY)) {
                    System.out.print("·");
                } else {
                    System.out.print(cells[i][j].toString());
                }
            }
            System.out.println("");
        }
    }
    
    public Board getCopy(){
        Board b = new Board(ROWS, COLUMNS, NUMBER_OF_MARKS);
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                b.put(new Cell(i, j, getCell(i, j)));
            }
        }
        return b;
    }
    
}
