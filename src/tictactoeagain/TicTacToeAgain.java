package tictactoeagain;

//import java.io.FileNotFoundException;

/*
MEGYJEGYZÉSEK

A PROGRAM RÖVIDEN

Két fajta játékost írtam: egy emberek számára konzolról irányíthót, és egy 
gépit, ami egy fát épít a lehetséges lépésekből, és az alapján minimax 
algoritmussal dönt. Indításkor beállíthatjuk, hogy kik játszanak, valamint a 
gépi játékos egyes paramétereit. A játék addig megy, amíg vki nyer, vagy 
betelik a tábla.


*/
public class TicTacToeAgain {

    public static void main(String[] args){
        
        System.out.println("\n-------------  AMŐBA  -------------\n");
        UserInterface.start();
        if( !UserInterface.isForceQuit ){
            runSimulation();
        }
        
        
        
        //teszteléshez van/maradt külön metódus
        //testMode();
        
    }
    
    public static void runSimulation(){
        Board board = new Board(UserInterface.boardRow, UserInterface.boardColumn, UserInterface.boardMark);
        Player player1, player2;
        boolean hasWinner = false;
        if(UserInterface.isHuman1){
            player1 = new HumanPlayer(PlayerType.X);
        }else{
            player1 = new MinimaxPlayer(PlayerType.X, UserInterface.ai1TimeOut, UserInterface.ai1Difficulty);
        }
        if(UserInterface.isHuman2){
            player2 = new HumanPlayer(PlayerType.O);
        }else{
            player2 = new MinimaxPlayer(PlayerType.O, UserInterface.ai2TimeOut, UserInterface.ai2Difficulty);
        }
        
        for (int i = 0; i < board.ROWS * board.COLUMNS; i++) {
            System.out.println("---------------------------------------------");
            Cell nextCell;
            PlayerType p;
            board.printNiceBoard();
            if( i % 2 == 0){
                p = player1.getMyType();
                nextCell = player1.nextMove(board);
            }else{
                p = player2.getMyType();
                nextCell = player2.nextMove(board);
            }
            
            board.put(nextCell);
            System.out.println( (i+1) + ". kör: " + p.toString() + " : " +  nextCell );
            if(board.hasWon(p)){
                System.out.println( p.toString() + " nyert.");
                hasWinner = true;
                break;
            }
        }
        //end
        board.printNiceBoard();
        if(!hasWinner){
            System.out.println("Döntetlen.");
        }
        if(player1 instanceof MinimaxPlayer){
        ((MinimaxPlayer) player1).endGame();
        }
        if(player2 instanceof MinimaxPlayer){
         ((MinimaxPlayer) player2).endGame();
        }
    }
 
    public static void testMode(){
        Board board = new Board(10, 10, 5);
        Player player1, player2;
        
        player1 = new MinimaxPlayer(PlayerType.X);
        player2 = new MinimaxPlayer(PlayerType.O);
        
        for (int i = 0; i < board.ROWS * board.COLUMNS; i++) {
            System.out.println("---------------------------------------------");
            Cell nextCell;
            PlayerType p;
            if( i % 2 == 0){
                p = player1.getMyType();
                nextCell = player1.nextMove(board);
            }else{
                p = player2.getMyType();
                nextCell = player2.nextMove(board);
            }
            
            board.put(nextCell);
            System.out.println( (i+1) + ". round: " + p.toString() + "'s step: " +  nextCell );
            board.printNiceBoard();
            if(board.hasWon(p)){
                System.out.println( p.toString() + " won.");
                break;
            }
        }
        System.out.println("-- END --");
        if(player1 instanceof MinimaxPlayer){
        ((MinimaxPlayer) player1).endGame();
        }
        if(player2 instanceof MinimaxPlayer){
         ((MinimaxPlayer) player2).endGame();
        }
    }
    
}
